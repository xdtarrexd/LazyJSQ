/**
 * Created by tarre on 2016-06-07.
 */
"use strict";
/*
 * Lazy JavaScriptQuery v 1.0.0
 * Av tarre
 *
 * */

/* Cached selector for jQUery*/
var $$ = new function () {
    var c = {};
    return function (n) {
        return c[n] ? c[n] : c[n] = $(n)
    }
};
/* Heavy data cache
 *
 * var heavydata = $$$.exists('name'); // Returns data if it exists. will check by name first
 *
 * if(!heavydata){
 *   var data = getHeavyData(....);
 *   var heavydata = $$$.set('name', data);
 * }
 * */
var $$$ = new function () {
    /* name */
    var cn = {};
    /* data */
    var cd = {};
    return {
        exists: function (n) {
            return cn[n] ? cd[n] : null;
        },
        set: function (n, v) {
            cn[n] = true;
            return cd[n] = v
        }
    }
};

/* Auto detects if object \ array */
var each = function (d, fCallback, i) {
    if (d.constructor === Array) { /* Array.isArray(d) is shit, it only determines shit and poop */
        for (i = !i ? 0 : i; i < d.length; i++) {
            fCallback(d[i]);
        }
        return;
    }
    for (var k in d) {
        fCallback(k, d[k]);
    }
};

var eachSplit = function (d, delimiter, fCallback, i) {
    return each(d.split(delimiter), fCallback, i);
};

/* Prototypes */
/*d*/
/**/
/* Ez group matching without aids, will work with any group fetching regex */
String.prototype.preg_match_all = function (regex) {
    var res = [];
    var m = regex.exec(this);
    if (regex.global) {
        do {
            if (!m) {
                return res;
            }
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            /* m[0] = Full string, so if > 2 then there is more than 1 captured group */
            res.push(m.length > 2 ? m : m[1]);

        } while ((m = regex.exec(this)) !== null);

    } else {
        /* Put whole array if there is more than 1 match*/
        return m.length > 2 ? m : m[1];
    }
    return res;
};

String.prototype.preg_replace = function (search, replacement) {
    return this.replace(new RegExp(search, 'g'), replacement);
};

/* Requires JavaScript 1.6 / ECMAScript 5 mangle me  */
Array.prototype.getUnique = function () {
    return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    });
};

/* If you want to split and assign the array as an object with names insteadof [0]*/
String.prototype.split2Obj = function (delimiter, aNames) {
    var d = this.split(delimiter), res = {};
    if (d.length > aNames.length) {
        console.error('Delimiter exceeded aNames length');
        return null;
    }
    else if(d.length < aNames.length)
    {
        console.error('aNames exceeded Delimiter length')
        return null;
    }
    for (var i = 0; i < d.length; i++) {
        res[aNames[i]] = d[i];
    }
    return res;
};